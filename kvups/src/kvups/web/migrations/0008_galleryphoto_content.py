# -*- coding: utf-8 -*-
# Generated by Django 1.11.15 on 2019-12-14 08:21
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0007_auto_20191214_0752'),
    ]

    operations = [
        migrations.AddField(
            model_name='galleryphoto',
            name='content',
            field=models.TextField(default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
