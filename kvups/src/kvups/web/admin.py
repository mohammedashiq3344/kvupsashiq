from __future__ import unicode_literals
from django.contrib import admin
from web.models import Slider, Achievement, Photo, Testimonial, News, Facility, Activity, VideoGallery

class SliderAdmin(admin.ModelAdmin):
	list_display = ('id','image')
	
admin.site.register(Slider,SliderAdmin)


class AchievementAdmin(admin.ModelAdmin):
	list_display = ('heading','sub_heading','content','image')
	
admin.site.register( Achievement,AchievementAdmin)


class PhotoAdmin(admin.ModelAdmin):
	list_display = ('id','image')
	
admin.site.register(Photo,PhotoAdmin)


class TestimonialAdmin(admin.ModelAdmin):
	list_display = ('image','name','designation','content')
	
admin.site.register( Testimonial,TestimonialAdmin)


class NewsAdmin(admin.ModelAdmin):
	list_display = ('heading','sub_heading','content','image','time')
	
admin.site.register( News,NewsAdmin)


class FacilityAdmin(admin.ModelAdmin):
	list_display = ('heading','content','image')
	
admin.site.register( Facility,FacilityAdmin)


class ActivityAdmin(admin.ModelAdmin):
	list_display = ('heading','sub_heading','content','image')
	
admin.site.register( Activity,ActivityAdmin)


class VideoGalleryAdmin(admin.ModelAdmin):
	list_display = ('heading','video_url')
	
admin.site.register( VideoGallery,VideoGalleryAdmin)