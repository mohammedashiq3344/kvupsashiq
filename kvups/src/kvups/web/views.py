from __future__ import unicode_literals   
from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.http.response import HttpResponseRedirect, HttpResponse
from web.models import Slider,Achievement,Photo,Testimonial,News,Facility,Activity,VideoGallery


def index(request):
	slider_datas = Slider.objects.all()
	achievement_datas = Achievement.objects.all()
	photo_datas = Photo.objects.all()
	testimonial_datas = Testimonial.objects.all()
	news_datas = News.objects.all()
	facility_datas = Facility.objects.all()
	activity_datas = Activity.objects.all()
	videogallery_datas = VideoGallery.objects.all()
	context = {
		"title" : "KVUPS",
		"caption" : "KVUPS CAPTION",
		"slider_datas" : slider_datas,
		"achievement_datas" : achievement_datas,
		"photo_datas" : photo_datas,
		"testimonial_datas" : testimonial_datas,
		"news_datas" : news_datas,
		"facility_datas" : facility_datas,
		"activity_datas" : activity_datas,
		"videogallery_datas" : videogallery_datas
	}
	return  render(request,'web/index.html',context)
 


