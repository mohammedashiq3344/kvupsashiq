from __future__ import unicode_literals
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Slider(models.Model):
	image = models.ImageField(upload_to="web/slider/")

	class meta:
		db_table = 'web_slider'
		verbose_name = "slider"
		verbose_name_plural = "sliders"

	def __unicode__(self):
		return self.image.url


class Achievement(models.Model):
	heading = models.CharField(max_length=128)
	sub_heading = models.CharField(max_length=128)
	content = models.TextField()
	image = models.ImageField(upload_to="web/achievement/")

	class meta:
		db_table = 'web_achievement'
		verbose_name = "achievement"
		verbose_name_plural = "achievements"

	def __unicode__(self):
		return self.heading


class Photo(models.Model):
	image = models.ImageField(upload_to="web/photo/")

	class meta:
		db_table = 'web_photo'
		verbose_name = "photo"
		verbose_name_plural = "photos"

	def __unicode__(self):
		return self.image.url


class Testimonial(models.Model):
	image = models.ImageField(upload_to="web/testimonial/")
	name = models.CharField(max_length=128)
	designation = models.CharField(max_length=128)
	content = models.TextField()
	
	class meta:
		db_table = 'web_testimonial'
		verbose_name = "testimonial"
		verbose_name_plural = "testimonials"

	def __unicode__(self):
		return self.name


class News(models.Model):
	image = models.ImageField(upload_to="web/news/")
	heading = models.CharField(max_length=128)
	sub_heading = models.CharField(max_length=128)
	content = models.TextField()
	time = models.DateTimeField(auto_now=True)
	
	class meta:
		db_table = 'web_news'
		verbose_name = "news"
		verbose_name_plural = "newss"

	def __unicode__(self):
		return self.heading


class Facility(models.Model):
	image = models.ImageField(upload_to="web/facility/")
	heading = models.CharField(max_length=128)
	content = models.TextField()

	class meta:
		db_table = 'web_facility'
		verbose_name = "facility"
		verbose_name_plural = "facilitys"

	def __unicode__(self):
		return self.heading


class Activity(models.Model):
	image = models.ImageField(upload_to="web/activity/")
	heading = models.CharField(max_length=128)
	sub_heading = models.CharField(max_length=128)
	content = models.TextField()

	class meta:
		db_table = 'web_activity'
		verbose_name = "activity"
		verbose_name_plural = "activitys"

	def __unicode__(self):
		return self.heading


class VideoGallery(models.Model):
	video_url = models.URLField()
	heading = models.CharField(max_length=128)

	class meta:
		db_table = 'web_videogallery'
		verbose_name = "videogallery"
		verbose_name_plural = "videogallerys"

	def __unicode__(self):
		return self.heading